# README #

## One Summer Chicago Youth Bank Map ##

 * version 1.0

### How to make your own map ###

**index.html**

 * add your Google API key ( "YOUR_KEY_HERE" )
 * add your Google Analytics account information ( "YOUR_GA_HERE" )
  
**bankmap.js**

 * add Fusion Table IDs ( "YOUR_ID_HERE" ) 
 * add Google API key ( "YOUR_KEY_HERE" )
 * change queries to match columns in your fusion table

### Technologies ###

 * [Google Fusion Tables](https://support.google.com/fusiontables/answer/184641)
 * [Google Maps](https://developers.google.com/maps/documentation/javascript/)
 * [jQuery](http://jquery.com/)
 * [Bootstrap](http://getbootstrap.com/)



