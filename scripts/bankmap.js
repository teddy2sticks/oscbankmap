//bank locator


var fusionTableId 		    = "YOUR_ID_HERE" ; // Banks March 2017
var atmfusionTableId 		= "YOUR_ID_HERE" ; // ATM March 2017
var googleAPIkey          = getGoogleAPIkey();
var googleAPIurl          = "https://www.googleapis.com/fusiontables/v1/query";
var map;
var geocoder;
var chicago;
var addrMarker              = null;
var radiusLoc               = null;
var geoaddress            	= null;  // geocoded pin placement address
var searchRadius            = null;  // in meters 1609=1 mile, 804=1/2 mile
var searchRadiusCircle      = null;
var searchtype              = null;  // radius, all, atm
var addrMarkerImage       	= 'images/yellow-pin-lg.png';
var markersArray						= []; 	 // for the marker array
var infoWindow						  = null;  // infowindow
var latlngbounds        		= null;  // for panning and zooming to include all searched markers
var selectedSchoolID				= null;  // passing of info for poping selected school infowindow
var searchtype							= null;  // allschools, oneschool, address



function initializeMap() {
	//clearSearch();
	clearMapFilters();
	var grayStyles = [
	  {
	    "featureType": "road",
	    "elementType": "geometry.fill",
	    "stylers": [
	      { "lightness": 1 },
	      { "saturation": -100 }
	    ]
	  },{
	    "featureType": "road.highway.controlled_access",
	    "elementType": "geometry.stroke",
	    "stylers": [
	      { "saturation": -100 },
	      { "visibility": "off" }
	    ]
	  },{
	    "featureType": "road",
	    "elementType": "geometry.stroke",
	    "stylers": [
	      { "visibility": "off" }
	    ]
	  },{
	    "featureType": "road.local",
	    "elementType": "geometry.fill",
	    "stylers": [
	      { "color": "#808080" },
	      { "lightness": 50 }
	    ]
	  },{
	    "featureType": "road",
	    "elementType": "labels.text.stroke",
	    "stylers": [
	      { "saturation": -100 },
	      { "gamma": 9.91 }
	    ]
	  },{
	    "featureType": "landscape",
	    "stylers": [
	      { "saturation": -70 }
	    ]
	  },{
	    "featureType": "administrative",
	    "stylers": [
	      { "visibility": "on" }
	    ]
	  },{
	    "featureType": "poi",
	    "stylers": [
	      { "saturation": -50 }
	    ]
	  },{
	    "featureType": "road",
	    "elementType": "labels",
	    "stylers": [
	      { "saturation": -70 }
	    ]
	  },{
	    "featureType": "transit",
	    "stylers": [
	      { "saturation": -70 }
	    ]
	  }
	];
		geocoder                = new google.maps.Geocoder();
		chicago					= new google.maps.LatLng(41.839, -87.67); // default center of map
    var myOptions = {
			styles: grayStyles,
	        zoom: 10,
	        center: chicago,
	        disableDefaultUI: true,
	        scrollwheel: false,
	        navigationControl: true,
	        panControl: false,
	        zoomControl: true,
	        scaleControl: false,
			    mapTypeControl: true,
				  mapTypeControlOptions: {
	      			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
					    position: google.maps.ControlPosition.RIGHT_BOTTOM
					},
	 			zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.RIGHT_CENTER
					},
	        //navigationControlOptions: {style: google.maps.NavigationControlStyle.LARGE },
	        mapTypeId: google.maps.MapTypeId.ROADMAP //TERRAIN
	    };

	    map = new google.maps.Map($("#map_canvas")[0], myOptions);

			//hold mouse down for 1.5 seconds to trigger radius search
			new LongPress(map, 1500);
			google.maps.event.addListener(map, 'longpress', function(event) {
				addrFromLatLng(event.latLng, "longpress");
			});

			//change placeholder text for smaller screens
			$(function() {
				var txt = $(window).width() < 375 ? 'Home, school or work address' : 'Enter your home, school or work address';
				$('#txtSearchAddress').attr('placeholder', txt);
			});

			//searchfromurl();
		  // display all the locations
		  mapQueryAll();

}


function clearMapFilters() {
  // $('#filterOptions').collapse('hide');
	// $('#filterATM').collapse('hide');
	$("#age15").prop("checked", false);
  $("#age16").prop("checked", false);
  $("#age17").prop("checked", false);
  $("#age18").prop("checked", true);
	$("#idTypeAll").prop("checked",  true);
  $("#idTypeGov").prop("checked",  false);
  $("#idTypeSchool").prop("checked",  false);
	$("#minMonthlyBalanceSlider").bootstrapSlider('setValue', 0);
	$("#openingBalanceSlider").bootstrapSlider('setValue', 0);
}

// not used
// URL search:  ?Address=123+N+Western
// Looks at URL for ? and determines what to display
function searchfromurl() {
  var pageurl = top.location.href
  var q = pageurl.split('=')[1];
  if(q !== undefined){
    var nq = q.replace(/\+/g, " ");
		$("#txtSearchAddress").val(nq) ;
		addressSearch();
  }else{
    // q is undefined - not a url search
    mapQueryAll();
  }

}


// 	function mapQueryAll2() {
// 		clearSearch();
// 	  $("#txtSearchAddress").val('');
// 		searchtype = "all";
// 		var whereClause = filterBanks();
// 		whereClause += " ORDER BY 'BranchStreetAddress'";
//
// 	layerMap1 = new google.maps.FusionTablesLayer({
// 		query: {
// 			from:   fusionTableId,
// 			select: "BranchStreetAddress",
// 			where:  whereClause
// 		}
// 		, suppressInfoWindows: false
// 	});
//
// layerMap1.setMap(map);
// }


  // AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, AdultCoSigner, OpenAccountOnline, Bank, BranchName, ATM, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude
// called from initialize script and update btn on filters // "all" displays all locations
function mapQueryAll() {
	clearSearch("keepMyInfoOpen");
  $("#txtSearchAddress").val('');
	searchtype = "all";
	var whereClause = filterBanks();
	whereClause += " ORDER BY 'BranchStreetAddress'";
	var query = "SELECT AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, Bank, BranchName, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude FROM " + fusionTableId + whereClause;
	encodeQuery(query, createMarkers);
}


function mapQueryATM(type) {
	clearSearch();
  $("#txtSearchAddress").val('');
	searchtype = "atm";
	var n = getBankATMNetwork(type);
	query = "SELECT BankATMNetwork, Type, ATM, StreetAddress, City, State, Zipcode, Latitude, Longitude FROM "+ atmfusionTableId + " WHERE BankATMNetwork = '"+n+"' ORDER BY ATM ";
	encodeQuery(query, createMarkersATM);
}


function addressSearch(gaddress) {
	var theInput = $.trim( $("#txtSearchAddress").val().toUpperCase() );
  var address = theInput;
  if (address !== "" ) {
		clearSearch();
    if (address.toLowerCase().indexOf("chicago, illinois") === -1) {
      address = address + " chicago, illinois";
    }
    geocoder.geocode({ 'address': address }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        geoaddress = (results[0].formatted_address);
				radiusLoc  =  results[0].geometry.location;
        map.setCenter(radiusLoc);
        if (addrMarker) { addrMarker.setMap(null); }
          addrMarker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: map,
          icon: addrMarkerImage,
          animation: google.maps.Animation.DROP,
          title: geoaddress
        });
				searchtype = "radius";
				$("#txtSearchAddress").val(geoaddress);
        //_trackClickEventWithGA("Search", "Raduis", geoaddress);
				getSearchRadius();
				drawSearchRadiusCircle(radiusLoc);
				var whereClause = filterBanks();
        whereClause += " AND ST_INTERSECTS('Latitude', CIRCLE(LATLNG" +  radiusLoc.toString() + "," + searchRadius + "))";
        	var query = "SELECT AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, Bank, BranchName, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude FROM " + fusionTableId + whereClause;
        encodeQuery(query, createMarkers);

      } else {//geocoder status not ok
        alert("We could not find your address: " + status);
      }
    });
  } else {//didn't enter an address
    alert("Please enter an address.");
  }

}


// radius search displays locations within a radius
// filtering takes place
function radiusSearch() {
  searchtype = "radius";
  var address = geoaddress;
  if (address != "") {
    getSearchRadius();
    drawSearchRadiusCircle(radiusLoc);
    var whereClause = filterBanks();
		whereClause += " AND ST_INTERSECTS('Latitude', CIRCLE(LATLNG" +  radiusLoc.toString() + "," + searchRadius + "))";
		whereClause += " ORDER BY 'BranchStreetAddress'";
		var query = "SELECT AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, Bank, BranchName, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude FROM " + fusionTableId + whereClause;
		encodeQuery(query, createMarkers);
  } else {//fail
    alert("Search failed. Please reset the map and try again.");
  }
}


// called from the findMe button to geocode the address
// and from a longpress on the map
// produces a pindrop and address/radius search
// try to combine with address search
function addrFromLatLng(latLngPoint, longpress) {
  clearSearch();
  //clearMapFilters();
  searchtype = "radius";
  geocoder.geocode({'latLng': latLngPoint}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        geoaddress = (results[0].formatted_address);
        radiusLoc  =  results[0].geometry.location;
        //map.setZoom(12);
        if (addrMarker) { addrMarker.setMap(null); }
        	addrMarker = new google.maps.Marker({
	          position: results[0].geometry.location,
	          map: map,
	          icon: addrMarkerImage,
	          animation: google.maps.Animation.DROP,
	          title: geoaddress
        	});

       $('#txtSearchAddress').val(results[0].formatted_address);
				getSearchRadius();
				drawSearchRadiusCircle(radiusLoc);
				// if(longpress) {
        //   //_trackClickEventWithGA("Search", "LongPress", geoaddress);
        // }else{
        //   //_trackClickEventWithGA("Search", "FindMe", geoaddress);
        // }
				var whereClause = filterBanks();
        whereClause += " AND ST_INTERSECTS('Latitude', CIRCLE(LATLNG" +  radiusLoc.toString() + "," + searchRadius + "))";
        whereClause += " ORDER BY 'BranchStreetAddress'";
				var query = "SELECT AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, Bank, BranchName, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude FROM " + fusionTableId + whereClause;
        encodeQuery(query, createMarkers);
      }
    } else {
      alert("Geocoder failed due to: " + status);
    }
  });
}


// called from update btn on the filters panel
function filteredSearch(){
	  $('#filterOptions').collapse('hide');
		// $('#filterATM').collapse('hide');
		if(searchtype === "radius"){
			addressSearch(geoaddress)
		}else{
			mapQueryAll();
		}
}


function filterBanks() {
	var age15 = $("#age15").is(":checked");
	var age16 = $("#age16").is(":checked");
  var age17 = $("#age17").is(":checked");
  var age18 = $("#age18").is(":checked");
	var idTypeAll = $("#idTypeAll").is(":checked");
  var idTypeGov = $("#idTypeGov").is(":checked");
  var idTypeSchool = $("#idTypeSchool").is(":checked");
	var minMonthly = $("#minMonthlyBalanceSlider").bootstrapSlider('getValue');
	var openingBal = $("#openingBalanceSlider").bootstrapSlider('getValue');

	var ageFilterArray  = [];
	var ageFilterClause = "";
	if (age15) { ageFilterArray.push( " 'Below 16' " ); }
	if (age16) { ageFilterArray.push( " '16' " ); }
  if (age17) { ageFilterArray.push( " '17' " ); }
  if (age18) { ageFilterArray.push( " '18 to 24' " ); }
  if (ageFilterArray.length > 0) {
     ageFilterClause += " AgeCategory IN (";
     ageFilterClause += ageFilterArray.join(","); ageFilterClause += " )";
  }
	var idTypeFilterArray  = [];
	var idTypeFilterClause = "";
	if (idTypeAll) { idTypeFilterArray.push( " 'Gov ID', 'School ID' " ); }
  if (idTypeGov) { idTypeFilterArray.push( " 'Gov ID' " ); }
  if (idTypeSchool) { idTypeFilterArray.push( " 'School ID' " ); }
  if (idTypeFilterArray.length > 0) {
     idTypeFilterClause += " IDType IN (";
     idTypeFilterClause += idTypeFilterArray.join(","); idTypeFilterClause += " )";
  }

	var minFilterClause  = " MinMonthlyBalance >= " +minMonthly ;
	var openFilterClause = " OpeningDeposit >= "+openingBal ;

	var whereClause = " WHERE ";
	whereClause += ageFilterClause;
	whereClause += " AND " + idTypeFilterClause;
	whereClause += " AND " + openFilterClause;
	whereClause += " AND " + minFilterClause;

	return(whereClause);
}


function buildRadiusDropDown() {

    var binputrad = "" +
      "<div id='input-radius'>"+
        "<select id='ddlRadius' class='form-control input-xs' onchange='filteredSearch();' >"+
        "<option value='1609'>1 mile</option>"+
        "<option value='3218'>2 miles</option>"+
        "<option value='4828'>3 miles</option>"+
        "<option value='6437'>4 miles</option>"+
        "<option value='8046'>5 miles</option>"+
        "<option value='16090'>10 miles</option>"+
        "</select>"+
      "</div>";

      return(binputrad);
}


function getSearchRadius() {
  searchRadius = $("#ddlRadius").val();
  if (typeof searchRadius === "undefined") {
    searchRadius = "3218";
  }
  switch (searchRadius) {
    case "402":
        map.setZoom(15);
        break;
    case "804":
    case "1609":
        map.setZoom(14);
        break;
    case "2414":
    case "3218":
        map.setZoom(13);
        break;
    case "4023":
    case "4828":
    case "6437":
        map.setZoom(12);
        break;
    case "8046":
    case "9656":
        map.setZoom(11);
        break;
    case "16090":
        map.setZoom(10);
        break;
    default:
        map.setZoom(11);
    }

}

// My Info, ATM, Tips
function createButtonGroup() {
	var g = "<div style='float:right;'>"+
	"<button class='btn btn-blue btn-xs filteroptions' type='button' data-toggle='collapse' data-target='#filterOptions' aria-expanded='false' aria-controls='filterOptions'>My Info</button>"+
	"<button class='btn btn-warning btn-xs btnATM' type='button' data-toggle='collapse' data-target='#filterATM' aria-expanded='false' aria-controls='filterATM'>ATMs</button>"+
	"<button class='btn btn-purple btn-xs btnhelp' type='button' data-toggle='collapse' data-target='#helpinfo' aria-expanded='false' aria-controls='helpinfo'>Tips</button></div>";
	return g;
}

function resetVideo(vid){
	//console.log(vid);
	document.getElementById(vid).pause();
	document.getElementById(vid).currentTime = 0;
	document.getElementById(vid).play();
}



function drawSearchRadiusCircle(point) {
  if (searchRadiusCircle != null) {
    searchRadiusCircle.setMap(null);
  }
  var circleOptions = {
    strokeColor: "#4b58a6",
    strokeOpacity: 0.3,
    strokeWeight: 1,
    fillColor: "#4b58a6",
    fillOpacity: 0.05,
    map: map,
    center: point,
    clickable: false,
    zIndex: -1,
    radius: parseInt(searchRadius)
  };
  searchRadiusCircle = new google.maps.Circle(circleOptions);
}

// AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, AdultCoSigner, OpenAccountOnline, Bank, BranchName, ATM, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude
// creates markers and infowindow data
function createMarkers(d) {
	var ulist =  "" ;
	var ulistlength= "" ;
	if(  d.rows !== undefined ) {
		ulist 		= d.rows;
		ulistlength = d.rows.length;
		for (var i = 0; i < ulistlength; i++) {
			var mLat	 			    		= ulist[i][12];
			var mLong	 			  		  = ulist[i][13];
			var position	  			  = new google.maps.LatLng(mLat,mLong);
			var image       				= getImage(ulist[i][4]);
			var marker 		  			  = new google.maps.Marker({
				AgeCategory           : ulist[i][0],
				OpeningDeposit        : ulist[i][1],
				MinMonthlyBalance     : ulist[i][2],
				IDType                : ulist[i][3],
				Bank                  : ulist[i][4],
				BranchName            : ulist[i][5],
				BranchStreetAddress   : ulist[i][6],
				City                  : ulist[i][7],
				State                 : ulist[i][8],
				Zipcode               : ulist[i][9],
				PhoneNumber           : ulist[i][10],
				Website               : ulist[i][11],
				Latitude              : ulist[i][12],
				Longitude             : ulist[i][13],
				position              : position,
				rowid 			 	        : i,
				icon 					        : image,
				map 					        : map
			});

			latlngbounds.extend(position);
			var fn = markerClick(map, marker, infoWindow);
			google.maps.event.addListener(marker, 'click', fn);
			markersArray.push(marker);
			infoWindow	= new google.maps.InfoWindow();
		} // end loop
		createResultsList();
	}else{  // nothing returned from query
		//results = "<span style='color:red;'>No results. Please check your address or change the radius and search again.</span>";
		if ( searchtype === "radius") {
			var inputrad = buildRadiusDropDown();
			results = "<div id='locationcount'><span>0 locations within "  + inputrad + "</span>";
			radmsg  = "Please change the radius or the filters and search again.";
		}else{
			results = "<div id='locationcount'><span>0 locations</span>";
			radmsg  = "Please check the filters and search again.";
		}
		results += createButtonGroup();
		results += "<br /><span style='color:red;'>"+radmsg+"</span></div>";




		$("#resultList").html(results);
		$("#ddlRadius").val(searchRadius);
		//return;
	}

	setMapZoom();


}


// BankATMNetwork, Type, ATM, StreetAddress, City, State, Zipcode, Latitude, Longitude
// creates markers and infowindow data
function createMarkersATM(d) {
	var ulist =  "" ;
	var ulistlength= "" ;
	if(  d.rows !== undefined ) {
		ulist 		= d.rows;
		ulistlength = d.rows.length;
		for (var i = 0; i < ulistlength; i++) {
			var mLat	 			    		= ulist[i][7];
			var mLong	 			  		  = ulist[i][8];
			var position	  			  = new google.maps.LatLng(mLat,mLong);
			var image       				= getImage(ulist[i][0]);
			var marker 		  			  = new google.maps.Marker({
				BankATMNetwork        : ulist[i][0],
				Type                  : ulist[i][1],
				ATM                   : ulist[i][2],
				StreetAddress         : ulist[i][3],
				City                  : ulist[i][4],
				State                 : ulist[i][5],
				Zipcode               : ulist[i][6],
				Latitude              : ulist[i][7],
				Longitude             : ulist[i][8],
				position              : position,
				rowid 			 	        : i,
				icon 					        : image,
				map 					        : map
			});

			latlngbounds.extend(position);
			var fn = markerClick(map, marker, infoWindow);
			google.maps.event.addListener(marker, 'click', fn);
			markersArray.push(marker);
			infoWindow	= new google.maps.InfoWindow();
		} // end loop
		createResultsListATM();
	}else{  // nothing returned from query
		results = "<span style='color:red;'>No results. We could not find any locations. Try another ATM Network</span>";
		$("#resultList").html(results);
		$("#ddlRadius").val(searchRadius);
	}
	setMapZoom();
}


function createResultsList() {
  //console.log("createResultsList: "+markersArray.length);
  var results = "";
  if (markersArray) {
		//console.log(markersArray);
		// sort alphabetically by name
		// thanks to: http://stackoverflow.com/questions/14208651/javascript-sort-key-value-pair-object-based-on-value
		// markersArray = markersArray.sort(function (a, b) {
    // 	return a.name.localeCompare( b.name );
		// });
	  if ( searchtype === "radius") {
			var inputrad = buildRadiusDropDown();
			results += "<div id='locationcount'  class='clearfix'><span style='float:left;'>"+markersArray.length+" locations within "  + inputrad + "</span>";
		}else{
			results += "<div id='locationcount'  class='clearfix'><span style='float:left;'>"+markersArray.length+" locations</span>";
		}
		results += createButtonGroup();
		results +="</div>";



		//results += "<div id='locationcount'><span>"+markersArray.length+" locations</span><button id='btnHeatmap' class='btn btn-default btn-xs pull-right hidden' onclick='toggleHeatmap()' style='margin-right:10px;'>Heatmap</button><button id='btnSignups' class='btn btn-default btn-xs pull-right hidden' onclick='toggleSignupCircles()' style='margin-right:10px;'>Signups</button></div>";
    for (i in markersArray) {
      var linkcolor = getLinkColor(markersArray[i].type);
      results += "" +
			"<div id='resultList"+markersArray[i].rowid+"' class='resultsrow' onclick=' openInfoWindow(&quot;"+
	    markersArray[i].AgeCategory+"&quot;,&quot;"+
      markersArray[i].OpeningDeposit+"&quot;,&quot;"+
      markersArray[i].MinMonthlyBalance+"&quot;,&quot;"+
      markersArray[i].IDType+"&quot;,&quot;"+
			markersArray[i].Bank+"&quot;,&quot;"+
			markersArray[i].BranchName+"&quot;,&quot;"+
			markersArray[i].BranchStreetAddress+"&quot;,&quot;"+
			markersArray[i].City+"&quot;,&quot;"+
			markersArray[i].State+"&quot;,&quot;"+
			markersArray[i].Zipcode+"&quot;,&quot;"+
			markersArray[i].PhoneNumber+"&quot;,&quot;"+
			markersArray[i].Website+"&quot;,"+
      markersArray[i].Latitude+","+
      markersArray[i].Longitude+");  '>" +
      "<img src='" +markersArray[i].icon+ "' />" ;

      results +="<span style='color:"+linkcolor+ "; ' >"+markersArray[i].Bank+"&nbsp;&nbsp;"+markersArray[i].BranchStreetAddress+"</span>";

      results +="</div>" ;
    }

  }
	$("#resultList").html(results);
	$("#resultListContainer").scrollTop(0);
	$("#ddlRadius").val(searchRadius);
}


function createResultsListATM() {
  //console.log("createResultsList: "+markersArray.length);
  var results = "";
  if (markersArray) {
			results += "<div id='locationcount'><span>"+markersArray.length+" locations</span>";
			results += createButtonGroup();
			results +="</div>";
			//	BankATMNetwork, Type, ATM, StreetAddress, City, State, Zipcode, Latitude, Longitude
    for (i in markersArray) {
      var linkcolor = getLinkColor(markersArray[i].type);
      results += "" +
			"<div id='resultList"+markersArray[i].rowid+"' class='resultsrow' onclick=' openInfoWindowATM(&quot;"+
	    markersArray[i].BankATMNetwork+"&quot;,&quot;"+
      markersArray[i].Type+"&quot;,&quot;"+
      markersArray[i].ATM+"&quot;,&quot;"+
      markersArray[i].StreetAddress+"&quot;,&quot;"+
			markersArray[i].City+"&quot;,&quot;"+
			markersArray[i].State+"&quot;,&quot;"+
			markersArray[i].Zipcode+"&quot;,"+
      markersArray[i].Latitude+","+
      markersArray[i].Longitude+");  '>" +
      "<img src='" +markersArray[i].icon+ "' />" ;
      results +="<span style='color:"+linkcolor+ "; ' >"+markersArray[i].ATM+"&nbsp;&nbsp;"+markersArray[i].StreetAddress+"</span>";
      results +="</div>" ;
    }
  }
	$("#resultList").html(results);
	$("#resultListContainer").scrollTop(0);
	$("#ddlRadius").val(searchRadius);
}

// AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, AdultCoSigner, OpenAccountOnline, Bank, BranchName, ATM, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude
// populates the info window
// called from markerclick and resultslist click
function openInfoWindow(AgeCategory, OpeningDeposit, MinMonthlyBalance, IDType, Bank, BranchName, BranchStreetAddress, City, State, Zipcode, PhoneNumber, Website, Latitude, Longitude) {
  var sposition	  = new google.maps.LatLng(Latitude,Longitude);
  var headcolor   = getLinkColor();
  var contents = "<div class='googft-info-window' style='color:" + headcolor + ";'>" +
		"<h4>" + BranchName + "</h4>" +
		"<p>" + BranchStreetAddress + "<br />" +
		PhoneNumber + "<br />" +
		"<a class='link-website' target='_blank' href='http://" + Website + "'>" + Website + "</a></p>" ;
  contents += "<div class='directionsdiv'>" ;
  var startaddr = "";
  if (addrMarker !== null) {
    startaddr = "saddr="+ geoaddress + "&";
  }
  var destaddr = "daddr="+BranchStreetAddress;//.replace(" ", "+");
	contents +=	"<a class='link-get-directions'  style='color:" + headcolor + "' href='http://maps.google.com/maps?";
  contents += startaddr + destaddr + "' target='_blank' >Get directions</a><br />"	;
	contents +=	"</div></div>";
  infoWindow.setOptions({
        pixelOffset: new google.maps.Size(0, -34)
       });
	infoWindow.setContent(contents);
	infoWindow.setPosition(sposition);
	infoWindow.open(map);
  map.panTo(sposition);
  positionMarkersOnMap();
}

//	BankATMNetwork, Type, ATM, StreetAddress, City, State, Zipcode, Latitude, Longitude
// called from markerclick and resultslist click on ATM search
function openInfoWindowATM(BankATMNetwork, Type, ATM, StreetAddress, City, State, Zipcode, Latitude, Longitude) {
  var sposition	  = new google.maps.LatLng(Latitude,Longitude);
  var headcolor   = getLinkColor();
  var contents = "<div class='googft-info-window' style='color:" + headcolor + ";'>" +
		"<h4>" + ATM + "</h4>" +
		"<p>" + StreetAddress + "<br />" +
		City+ ", " + State + " " + Zipcode + "</p>" ;
  contents += "<div class='directionsdiv'>" ;
  var startaddr = "";
  if (addrMarker !== null) {
    startaddr = "saddr="+ geoaddress + "&";
  }
  var destaddr = "daddr="+StreetAddress;//.replace(" ", "+");
	contents +=	"<a class='link-get-directions'  style='color:" + headcolor + "' href='https://maps.google.com/maps?";
  contents += startaddr + destaddr + "' target='_blank' >Get directions</a><br />"	;
	contents +=	"</div></div>";
  infoWindow.setOptions({
        pixelOffset: new google.maps.Size(0, -34)
       });
	infoWindow.setContent(contents);
	infoWindow.setPosition(sposition);
	infoWindow.open(map);
  map.panTo(sposition);
  positionMarkersOnMap();
}



// popup infowindow called when user clicks on a marker on the map
function markerClick(map, m, ifw) {

  return function() {
		//_trackClickEventWithGA("Click", "Marker on Map", m.BranchName);
		if(searchtype != "atm"){
    	openInfoWindow(m.AgeCategory, m.OpeningDeposit, m.MinMonthlyBalance, m.IDType,  m.Bank, m.BranchName,  m.BranchStreetAddress, m.City, m.State, m.Zipcode, m.PhoneNumber, m.Website, m.Latitude, m.Longitude);
		}else{
			openInfoWindowATM(m.BankATMNetwork, m.Type, m.ATM, m.StreetAddress, m.City, m.State, m.Zipcode, m.Latitude, m.Longitude);
		}
	};
}


function isMarkerImage(){
  if (markersArray) {
    if($.type( markersArray[0].icon ) === "string") {
     return true; // marker is an image
    } else {
      return false; // marker is an object - circles
    }
  }
}



function setMapZoom() {
	if (searchtype === "all") {
		map.setCenter(chicago);
		//map.fitBounds(latlngbounds);
		//map.panTo(chicago);
	}	else if (searchtype === "atm") {
			map.fitBounds(latlngbounds);
      //map.panTo(addrMarker.position);
	}	else if (searchtype === "radius") {
			//map.fitBounds(latlngbounds);
	     map.panTo(addrMarker.position);
	}

	positionMarkersOnMap();
	/*if(map.getZoom()>17){
		map.setZoom(17);
	}*/
}


// clear map items before doing a search
function clearSearch(ko) {

	if (infoWindow) {
		infoWindow.close(map);
    }
	if (addrMarker !== null){
    addrMarker.setMap(null);
    }
	if (searchRadiusCircle != null){
    searchRadiusCircle.setMap(null);
	}
	deleteMarkers();

  addrMarker = null;
  geoaddress = null;
	searchtype = null;
  searchRadiusCircle = null;
	latlngbounds = new google.maps.LatLngBounds(null);

	//radiusLoc = null;
	if (!ko){
		 $('#filterOptions').collapse('hide');
	}
	 $('#filterATM').collapse('hide');
	 $('#helpinfo').collapse('hide');


}


function isMobile() {
	if( $( window ).width() > 767 ) {
		return false;
	}else{
		return true;
	}
}

//centers the markers on the right side of the viewport on larger displays
//centers the markers on the middle bottom of the screen on mobile displays
function positionMarkersOnMap() {
  if( $( window ).width() > 767 ) {
    map.panBy(-calcPinLocationWidth(), 0);
  } else {
		map.panBy(0 , -calcPinLocationHeight() ) ;
  }
}


function calcPinLocationWidth() {
  var w=$( window ).width() / 4;
  return(w);
}


function calcPinLocationHeight() {
  var h=$( window ).height() / 2.5;
  return(h);
}


function getBankATMNetwork(stype) {
	var mytype = "";
	if( stype === "TCF" ) {
		mytype = "TCF";
	}else if (stype === "PNC"){
		mytype = "PNC";
	}else if (stype === "FM"){
		mytype = "First Midwest Bank";
	}else if (stype === "MB"){
		mytype = "MB Financial";
	}else if (stype === "W"){
		mytype = "Wintrust";
	}else if (stype === "RB"){
		mytype = "Republic Bank";
	}else if (stype === "MA"){
		mytype = "Marquette Bank";
	}
  return mytype;
}


function getLinkColor() {
	var linkcolor = "#333";
  return linkcolor;
}


function getImage(bank) {
  var image = "images/grey.png";
	if( bank === "TCF Bank" || bank === "TCF"  ) {
		image = "images/tcf.png";
	}else if (bank === "PNC"){
		image = "images/pnc.png";
	}else if (bank === "First Midwest Bank"){
		image = "images/fm.png";
	}else if (bank === "MB Financial"){
		image = "images/mb.png";
	}else if (bank === "Wintrust"){
		image = "images/w.png";
	}else if (bank === "Republic Bank"){
		image = "images/rb.png";
	}else if (bank === "Marquette Bank"){
		image = "images/ma.png";
	}
	  return image;
}



// lists the markers from the map
function listMarkers() {
  if (markersArray) {
    for (i in markersArray) {
      console.log(markersArray[i].sid);
    }
  }
}


// Removes the markers from the map, but keeps them in the array
function clearMarkers() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
  }
}


// Shows any markers currently in the array
function showMarkers() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(map);
    }
  }
}


// Deletes all markers in the array by removing references to them
function deleteMarkers() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}


// reset btn
function resetmap() {
	var pageurl = top.location.href
	if(pageurl.indexOf("?") >=0) {
		var x = pageurl.split('?')[0];
		top.location.href = x;
	} else {
		$("#txtSearchAddress").val('');
		initializeMap();
	}
	//hopscotch.endTour();
}


function ajaxerror() {
	alert("Your search cannot be executed. Please click Reset and try again.");
}


function closeOpenThings() {
	//var $myGroup = $('#search');
	$('#search').find('.collapse.in').collapse('hide');
}


// adds commas to a string
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


// encodes the query, returns json, and calls sf if success
function encodeQuery(q,sf) {
	var encodedQuery = encodeURIComponent(q);
		var url = [googleAPIurl];
		url.push('?sql=' + encodedQuery);
		url.push('&key='+ googleAPIkey);
		url.push('&callback=?');
		$.ajax({
			url: url.join(''),
			dataType: "jsonp",
			success: sf,
			error: function () {alert("AJAX ERROR for " + q ); }
		});
}




function findMe() {
  var foundLocation;
  if(navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(function(position) {
      foundLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      addrFromLatLng(foundLocation);
    }, null);
  } else {
    alert("Sorry, we could not find your location.");
  }
}


// LongPress on the map calls the
// thanks to stackoverflow - Leiko
// http://stackoverflow.com/questions/6264853/using-longclick-taphold-with-google-maps-in-jquery-mobile
function LongPress(map, length) {
  this.length_ = length;
  var me = this;
  me.map_ = map;
  me.timeoutId_ = null;
  google.maps.event.addListener(map, 'mousedown', function(e) {
    me.onMouseDown_(e);
  });
  google.maps.event.addListener(map, 'mouseup', function(e) {
    me.onMouseUp_(e);
  });
  google.maps.event.addListener(map, 'drag', function(e) {
    me.onMapDrag_(e);
  });
}

LongPress.prototype.onMouseUp_ = function(e) {
  clearTimeout(this.timeoutId_);
};

LongPress.prototype.onMouseDown_ = function(e) {
  clearTimeout(this.timeoutId_);
  var map = this.map_;
  var event = e;
  this.timeoutId_ = setTimeout(function() {
    google.maps.event.trigger(map, 'longpress', event);
  }, this.length_);
};

LongPress.prototype.onMapDrag_ = function(e) {
  clearTimeout(this.timeoutId_);
};


function getGoogleAPIkey() {
  var pageurl = top.location.href;
  if(pageurl.indexOf("file:///") >=0) {
    return ("YOUR_KEY_HERE"); //Local
  }else{
    return ("YOUR_KEY_HERE"); //Production
  }
}
